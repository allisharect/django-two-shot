from django.urls import path

from receipts.views import (
    ReceiptListView,
    AccountCreateView,
    ExpenseCategoryCreateView,
    ReceiptCreateView,
    AccountListView,
    ExpenseCategoryListView,
)

urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path(
        "accounts/create/",
        AccountCreateView.as_view(),
        name="account_new",
    ),
    path(
        "categories/create/",
        ExpenseCategoryCreateView.as_view(),
        name="expense_new",
    ),
    path(
        "create/",
        ReceiptCreateView.as_view(),
        name="receipt_new",
    ),
    path(
        "accounts/list/",
        AccountListView.as_view(),
        name="account_list",
    ),
    path(
        "categories/list/",
        ExpenseCategoryListView.as_view(),
        name="expense_list",
    ),
]
